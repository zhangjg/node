#+HTML_MATHJAX: path:"https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"
** 基礎
行內公式放在 ~$...$~ 之間, 獨立的公式放在 ~$$ ... $$~ 之間.

** 希臘字母表
#+Caption: 希臘字母表
#+ATTR_HTML: :align center
| 名稱    | 大寫     | 小寫                 |
|---------+----------+----------------------|
| /       | <        | <                    |
| <c>     | <c>      | <c>                  |
| alpha   | \Alpha   | \alpha               |
| beta    | \Beta    | \beta                |
| gamma   | \Gamma   | \gamma               |
| delta   | \Delta   | \delta               |
| epsilon | \Epsilon | \epsilon \varepsilon |
| zeta    | \Zeta    | \zeta                |
| theta   | \Theta   | \theta               |
| iota    | \Iota    | \iota                |
| kappa   | \Kappa   | \kappa               |
| lambda  | \Lambda  | \lambda              |
| mu      | \Mu      | \mu                  |
| nu      | \Nu      | \nu                  |
| xi      | \Xi      | \xi                  |
| omicron | \Omicron | \omicron             |
| pi      | \Pi      | \pi                  |
| rho     | \Rho     | \rho                 |
| sigma   | \Sigma   | \sigma               |
| tau     | \Tau     | \tau                 |
| upsilon | \Upsilon | \upsilon             |
| phi     | \Phi     | \phi \varphi         |
| chi     | \Chi     | \chi                 |
| psi     | \Psi     | \psi                 |
| omega   | \Omega   | \omega               |
** 上下標
上標使用 ~^{...}~ 表示, 如果上標只有一個字符可以省略大括號. 下標使用 ~_{...}~ 如果只有一個字符, 大括號也可以省略.

** 字符上添加符號

#+Caption: 字符上下添加符號
#+ATTR_HTML: :align center
| 示例              | 代碼              |
|-------------------+-------------------|
| /                 | <>                |
| <c>               | <c>               |
|-------------------+-------------------|
| $\bar{a}$         | ~\bar{a}~         |
| $\acute{a}$       | ~\acute{a}~       |
| $\check{a}$       | ~\check{a}~       |
| $\grave{a}$       | ~\grave{a}~       |
|-------------------+-------------------|
| $\hat{a}$         | ~\hat{a}~         |
| $\widehat{abc}$   | ~\widehat{abc}~   |
| $\tilde{a}$       | ~\tilde{a}~       |
| $\overline{ab}$   | ~\overline{ab}~   |
| $\widetilde{ab}$  | ~\widetilde{ab}~  |
| $\underline{ab}$  | ~\underline{ab}~  |
| $\ddot{o}$        | ~\ddot{o}~        |
| $\dot{o}$         | ~\dot{o}~         |
| $\overset{x}{b}$  | ~\overset{x}{b}~  |
| $\underset{x}{b}$ | ~\underset{x}{b}~ |

** 括號
1. 小括號, 中括號, 正常使用
2. 大括號需要用轉移表示 ~\{\}~ 這樣就不認爲是公司的分組了.
3. 尖括號, 使用 ~\langle~ 和 ~\rangle~, ~$\langle x \rangle$~: $\langle x \rangle$.
4. 向上取整數, 使用 ~\lceil~ 和 ~\rceil~, ~$\lceil x \rceil$~: $\lceil x \rceil$.
5. 向下取整數, 使用 ~\lfloor~ 和 ~\rfloor~, ~$\lfloor x \rfloor$~: $\lfloor x \rfloor$.

原始的括號不會隨着公式調整大小, 例如
$$
\{\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}\} \tag{1.3.1}
$$

可以使用 ~\left\{ ... \right\}~ 來替換.
$$
\left\{\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}\right\} \tag{1.3.2}
$$
** 數學函數符號

#+CAPTION: 數學函數和符号
#+ATTR_HTML: :align center
| 名稱       | 代碼                                                                                | 符號                                                                                 |
|------------+-------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------|
| 求和       | ~\sum~                                                                              | $\sum_{i=1}^n$                                                                       |
| 積分       | ~\int~                                                                              | $\int_{1}^{\infty}$                                                                  |
| 二重積分   | ~\iint~                                                                             | $\iint$                                                                              |
| 連乘       | ~\prod~                                                                             | $\prod$                                                                              |
| 比较符号   | ~\lt \gt \le \ge \neq \not\lt \ll \gg~                                              | $\lt  \gt \le \ge \neq \not\lt \ll \gg$                                              |
| 极限       | ~\lim_{x\to y}~ ~\underset{x\to 1}\lim~                                             | $\lim_{x \to 1}$ $\underset{x\to 1} \lim$                                            |
| 计算符号   | ~\times \div \pm \mp \cdot~                                                         | $\times  \div \pm \mp \cdot$                                                         |
| 可拼寫函數 | ~\sin, \max, \log \exp \ln~                                                         | $\sin, \max, \log \exp \ln$                                                          |
| 集合符号   | ~\cup \cap \setminus \subset \subseteq \subsetneq \in \notin \emptyset \varnothing~ | $\cup  \cap \setminus \subset \subseteq \subsetneq \in \notin \emptyset \varnothing$ |
| 组合       | ~{n+1 \choose 2k}~ 或 ~\binom{n+1}{2k}~                                             | ${n+1 +1 \choose 2k}$ 或 $\binom{n+1}{2k}$                                           |
| 箭头       | ~\to \rightarrow \leftarrow \Rightarrow \Leftarrow \mapsto~                         | $\to \rightarrow \leftarrow \Rightarrow \Leftarrow \mapsto$                          |
| 等号       | ~\approx~  ~\sim~ ~\simeq~ ~\cong~ ~\equiv~ ~\prec~                                 | $\approx \sim \simeq \cong \equiv \prec$                                             |
| 三角形     | ~\Delta~ ~\nabla~ ~\lhd~ ~\rhd~                                                     | $\Delta \nabla \lhd \rhd$                                                            |
| 因为,所以  | ~\because~ ~\therefore~                                                             | $\because \therefore$                                                                |
| 角         | ~\angle{ABC}~                                                                       | $\angle{ABC}$                                                                        |
** 分數和根式
分數第一種方法是使用 ~\frac{abc}{ddd}~ 單個字符的也可以省略大括號, 用 *空白隔開* ~\frac~ 以及分子和分母.

第二種方法, 把分子分母放到一個組中, 中間用 ~\over~ 分隔.例如 ~{ a+1 \over b+c}~: ${a+1 \over b+1}$.

根式用 ~\sqrt[n]{..}~ 表示, 如果是二次跟, ~[n]~ 不寫. 例如:$\sqrt[3] 5, \sqrt\frac{1}{2}$
** 字體
1. 表示數值集合的用~\mathbb~ 或 ~\Bbb~. 例如 $\mathbb R$ $\Bbb N$.
2. 表示矩陣的黑體, 使用 ~\mathbf{...}~, 例如: $\mathbf{ABCDEFG,abcdefg}$
3. 打印字體, 使用 ~\mathtt{...}~, 例如 $\mathtt{ABCDEFG,abcdefg}$
4. 羅馬字體, 使用 ~\mathrm{...}~, 例如 $\mathrm{ABCDEFG,abcdefg}$
5. 花體, 手写体, 使用 ~\mathscr{...}~, 例如 $\mathscr{ABCDEFG,abcdefg}$.
** 矩阵
=\begin{matrix}= 和 =\end{matrix}= 来创建矩阵, 行分割符用 =\\=, 元素之间的分割符 用 =&=.
这样元素会以矩阵的形式拍了, 如果要用圆括号把元素括起来, 使用 =pmatrix= , 替换 =matrix=.
方括号使用 =bmatrix=, 花括号用 =Bmatrix=, 竖线用 =vmatrix=, 双竖线用 =Vmatrix=.
