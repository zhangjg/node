* TensorFlow: 在多种分布系统上的大规模机器学习
** 编程模型和基本概念
一个 TensorFlow 计算被一个有向图描述, 有向图有节点集合组成. 图通过展开为 1) 一些种类的维护和更新持久状态的节点; 以及 2) 分叉, 循环控制结构. 客户端一般使用一种支持的外部语言 (C++ 或者 Python) 来构造计算图.

在 TensorFlow 图中, 每个 /节点/ 有〇个或多个输入, 且有〇个或多个输出. 节点表示一个 /操作/ 的具体化. 图中沿着常规边缘流动的值是 /张量/ , 也就是说一个任意维度的数组, 其值的类型被执行或者在构造图的时候被推测出来. 还存在着, 被称为 /控制依赖/ 的特殊边缘: 没有数据流沿着这些边缘, 但是他们暗示了在控制依赖目标节点开始执行之前, 其源节点必须完成执行.

*** 操作和核心
一个 /操作/ 有一个名字, 表示一个抽象的计算 (例如, "矩阵乘", 或 "加"). 一个操作可以有属性, 在图构造的时候, 所有的属性必须提供或推测出来, 以便实例化一个节点来执行这个操作. 属性通常被用来做不同类型的张量的转化操作 (例如, 把两个类型为 float 的张量通过按照int32 类型的张量加起来).

一个 /核心/ 是操作的一个特殊的实现, 可以运行在一个特定的设备上 (例如, CPU 或 GPU).

一个 TensorFlow 对通过注册机制, 定义了操作集合与可用的核心. 这个集合可以通过连接更多的操作以及/或者核心定义/注册来扩展.

*** 会话
客户端程序通过创建一个会话来和 TensorFlow 系统交互. 要创建一个计算图, 会话接口支持一个 /扩展/ 方法来增大被会话管理的图: 给图添加节点和边缘. 当会话被创建的时候最初的图是空的. 会话支持的其他的主要的操作是 /运行/. 

** 扩展
*** 梯度计算
很多机器学习的算法要计算模型的梯度, 所以 TensorFlow 中内建了自动的梯度计算. 如果 TensorFlow 图中依赖一个张量 C, 可能是通过一个复杂的子图的在张量 $\{X_{k}\}$ 上的操作得到的, 那么内建的函数就会返回张量 $\{ dC/dX_{k} \}$.


当 TensorFlow 需要计算张量 C 对 常量 I 的梯度的时候:

1. 从图中找到从 I 到 C 的路径
2. 回溯 I 到 C 的路径, 在回溯路径中的遇到的每个操作, 在 TensorFlow 图中添加一个节点, 作为梯度的一个分量.
3. 新添加的节点计算对应的正路径的操作的 "梯度函数". 梯度函数可能在任何操作上都被注册. 这些函数不仅把回溯路径计算出来的部分梯度作为输入, 也 (可能) 把正路径的输入输出作为输入.

** 部分执行
通常客户端需要执行整个执行图的一个子图. 为了支持这个要求, 一旦客户端把一个计算图设置到一个会话中之后, 我们的 *运行* 函数, 允许在这个图中的任意的子图上执行, 在图的任意的边缘上注入数据, 以及从图的任意边缘中取回数据.

图中的每个节点都有一个名字, 每个输出的节点都被源节点名字以及输出端口唯一确定. 输出端口从0 开始.

*Run* 被调用时, 两个参数可以帮助从计算图中定位要运行的子图:

1. Run 调用接收的输入中, 有一个可选映射 name:port 名字到 "fed" 张量的值.
2. 接收 ~output_names~ , 一个输出的列表 ~name[:port]~ 指定那些节点应该被执行. 如果端口部分提供了的话, 这个节点输出张量, 这个节点输出张量会被返回.
