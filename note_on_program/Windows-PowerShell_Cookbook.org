* Tour
1. Cmdlet Pattern
 #+BEGIN_QUOTE
 PowerShell introduces a powerful new type of command called a cmdlet (pronounced “command-let”). All cmdlets are
 named in a *Verb-Noun* pattern.
 #+END_QUOTE
2. 
 #+BEGIN_QUOTE
 Cmdlets and scripts should be named using a Verb-Noun syntax. The official guidance is that, with rare
 exception, cmdlets should use the standard PowerShell verbs.

 Verbs should be phrased in the present tense, and nouns should be singular.
 #+END_QUOTE
3. Standard Windows PowerShell common verbs
 | Verb   | Meanning                                                                                                                                                                       | Synonyms                                                                 |
 |--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------|
 | Add    | Adds a resource to a container or attaches an element to another element                                                                                                       | Append, Attach, Concatenate, Insert                                      |
 | Clear  | Removes all elements from a container                                                                                                                                          | Flush, Erase, Release, Unmark, Unset, Nullify                            |
 | Close  | Removes access to a resource                                                                                                                                                   | Shut, Seal                                                               |
 | Copy   | Copies a resource to another name or container                                                                                                                                 | Duplicate, Clone, Replicate                                              |
 | Enter  | Sets a resource as a context                                                                                                                                                   | Push, Telnet, Open                                                       |
 | Exit   | Returns to the context that was present before a new context was entered                                                                                                       | Pop, Disconnect                                                          |
 | Find   | Searches within an unknown context for a desired item                                                                                                                          | Dig, Discover                                                            |
 | Format | Converts an item to a specified structure or layout                                                                                                                            | Layout, Arrange                                                          |
 | Get    | Retrieves data                                                                                                                                                                 | Read, Open, Cat, Type, Dir, Obtain, Dump, Acquire, Examine, Find, Search |
 | Hide   | Makes a display not visible                                                                                                                                                    | Suppress                                                                 |
 | Join   | Joins a resource                                                                                                                                                               | Combine, Unite, Connect, Associate                                       |
 | Lock   | Locks a resource                                                                                                                                                               | Restrict, Bar                                                            |
 | Move   | Moves a resource                                                                                                                                                               | Transfer, Name, Migrate                                                  |
 | New    | Creates a new resource                                                                                                                                                         | Create, Generate, Build, Make, Allocate                                  |
 | Open   | Enables access to a resource                                                                                                                                                   | Release, Unseal                                                          |
 | Pop    | Removes an item from the top of a stack                                                                                                                                        | Remove, Paste                                                            |
 | Push   | Puts an item onto the top of a stack                                                                                                                                           | Put, Add, Copy                                                           |
 | Redo   | Repeats an action or reverts the action of an Undo                                                                                                                             | Repeat, Retry, Revert                                                    |
 | Remove | Removes a resource from a container                                                                                                                                            | Delete, Kill                                                             |
 | Rename | Gives a resource a new name                                                                                                                                                    | Ren, Swap                                                                |
 | Reset  | Restores a resource to a predefined or original state                                                                                                                          | Restore, Revert                                                          |
 | Select | Creates a subset of data from a larger data set                                                                                                                                | Pick, Grep, Filter                                                       |
 | Search | Finds a resource (or summary information about that resource) in a collection (does not actually retrieve the resource but provides information to be used when retrieving it) | Find, Get, Grep, Select                                                  |
 | Set    | Places data                                                                                                                                                                    | Write, Assign, Configure                                                 |
 | Show   | Retrieves, formats, and displays information                                                                                                                                   | Display, Report                                                          |
 | Skip   | Bypasses an element in a seek or navigation                                                                                                                                    | Bypass, Jump                                                             |
 | Split  | Separates data into smaller elements                                                                                                                                           | Divide, Chop, Parse                                                      |
 | Step   | Moves a process or navigation forward by one unit                                                                                                                              | Next, Iterate                                                            |
 | Switch | Alternates the state of a resource between different alternatives or options                                                                                                   | Toggle, Alter, Flip                                                      |
 | Unlock | Unlocks a resource                                                                                                                                                             | Free, Unrestrict                                                         |
 | Use    | Applies or associates a resource with a context                                                                                                                                | With, Having                                                             |
 | Watch  | Continually monitors an item                                                                                                                                                   | Monitor, Poll                                                            |
4. 
  #+BEGIN_QUOTE
  PowerShell lets you use the Tab key to autocomplete cmdlet names and parameter names.
  #+END_QUOTE
5. 
 #+BEGIN_QUOTE
 PowerShell is also case-insensitive.
 #+END_QUOTE
6. 
 #+BEGIN_QUOTE
 PowerShell supports positional parameters on cmdlets. Positional parameters let you provide
 parameter values in a certain position on the command line, rather than having to specify them by name.
 #+END_QUOTE
7. 
 #+BEGIN_QUOTE
 All PowerShell commands that produce output generate that output as objects as well.
 #+END_QUOTE
8. 
 #+BEGIN_QUOTE
 In PowerShell, variable names start with a *$* character.
 #+END_QUOTE
