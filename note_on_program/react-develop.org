* 依赖库
开发 Web 的时候, 虽然可以显示的依赖某些库, 但是最好还是用 webpack
把依赖库, 和自己的程序合并起来. 这样可以加快浏览器的加载.

对于开发过程中的依赖工具, 最好是安装到本地, 且作为开发依赖安装 *yarn* 的 *--dev* 选项. 这样的话, 可以减少对开发环境的依赖.

Reac Web 开发用到一下几个库:
1. react
2. react-dom
  =1= 和 =2= 都是 react web 开发的依赖库.
3. babel-cli
  这个库主要是因为要对 jsx 和 ES6 转码所以需要.
4. webpack
   这个库是用来把分散的库文件, 集合到一起, 成为一个统一的文件, 这样可以加快加载的速度.

** Babel 的用法
React Web 开发的时候, 需要的插件由 =babel-preset-env=, =babel-preset-react=, 在 babel 的配置文件 =.babelrc= 中, 需要做如下的配置:
#+BEGIN_SRC js
{
  "presets":["env","react"]
}
#+END_SRC
=env= 指明, 对于实验行的 Javascript 语法, 要做转码处理. =react= 是说对 =jsx= 代码要转换成 Javascript 代码.

Babel 的基本用法
#+BEGIN_SRC shell
$ npx babel src -d lib # code in *src* will compilation, targets put to *lib*
$ npx babel somefile.xxx -o somefile.js # compilate file *somefile.xxx*, get *somefile.js*
#+END_SRC

** Webpack
Webpack 解决的主要问题就是, 让浏览器可以像服务器端的程序那样, 使用 JS 库. 而不用在担心 Js 库加载时的异步问题.

Babel 编译后的文件生成的文件和源文件是由一一对应关系的, 对于多个文件的话, 最后在浏览器中运行的时候, 还需要解决库代码的依赖问题.

现在, 为 Web 开发, 比较好的解决库代码的依赖问题, 就是使用 Webpack.

Webpack 需要你制定程序的入口, 然后从这个入口开始分析, 整个的程序依赖了那些库, 把这些库和自己写的源代码一起写到目标文件中. 最后在页面上就不需要加载多个 JS 代码, 只要个 =script= 语句就可以了.

Webpack 的配置文件就是一个 js 文件, 内容一般如下:
#+BEGIN_SRC js
const path = require('path');

module.exports = {
  entry: './lib/test.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname,"dist")
  }
};
#+END_SRC

#+BEGIN_SRC shell
npx webpack --config webpack.config.js
#+END_SRC
