* socket.io 简介
Socket.io 是 WebSocket 的一个解决方法. Websocket 是在 HTTP/HTTPS 协议的基础上实现
Socket 通信的扩展协议. Socket.io 简化了 WebSocket 编程.

* socket.io 的一些概念
** 服务器
实现了 WebSocket 的服务器. 因为 Websocket 是对 HTTP/HTTPS 的扩展, 所以, 要实现 
ws/wss 协议, 需要提供 HTTP/HTTP 服务器.

这就是为什么, socket.io 要创建一个 服务器的时候, 需要提供 HTTP/HTTPS 服务器的原因.

** 基于事件
socket.io 为了方便编程, 是基于事件来编程的. 

通信事件有:
+ =connect=
+ =message=
+ =disconnet=

还是以通过 =server.emit('event name',{message:"can be a object"})= 或 
=client.emit()= 来自定义通信的事件.

** 名称空间
WebSocket 是在 HTTP/HTTPS 上扩展的, HTTP/HTTPS 是由路径的, 所以 WebSocket
也是由路径的.

socket.io 的名称空间, 对应的就是 Websocket 中的路径.
** 可以发送可以调式的消息
=socket.volatile.emit= 可以用来发送可以调式的信息.
** 消息被接收后确认
事件处理回掉函数的时候, 第一个参数是消息, 第二个参数是可选的 =done= 函数, 
这个 =done= 函数调用的时候, 被传入的函数, 回作为确认信息回传给 =.send= 或
 =.emit= 的回掉函数.


