(define numbered?
  (lambda (aexp)
    (cond
     ((atom? aexp) (number? aexp))
     ((eq? (car (cdr aexp) '+))
      (and
       (numbered? (car aexp))
       (numbered? (car (cdr (cdr aexp))))))
     ((eq? (car (cdr aexp) '-))
      (and
       (numbered? (car (cdr aexp)))
       (numbered? (car (cdr (cdr aexp))))))
     ((eq? (car (cdr aexp) '^\|))
      (and
       (numbered? (car (cdr aexp)))
       (numbered? (car (cdr (cdr aexp))))))
     (else #f))))
    
(define value
  (lambda (nexp)
    (cond
     ((atom? nexp)
      (cond
       ((number? nexp) nexp)
       (else #f)))
     ((eq? '+ (car (cdr nexp)))
      (+ (value (car nexp))
         (value (car (cdr (cdr nexp))))))
     ((eq? 'x (car (cdr nexp)))
      (x
       (value (car nexp))
       (value (car (cdr (cdr nexp))))))
     ((eq? '^ (car (cdr nexp)))
      (^ (value (car nexp))
         (value (car (cdr (cdr nexp)))))))))

(define set?
  (lambda (lat)
    (cond
     ((null? lat) #t)
     ((member? (car lat) (cdr lat)) #f)
     (else
      (set?
       (cdr lat))))))

(define makeset
  (lambda (lat)
    (cond
     ((null? lat) '())
     ((member? (car lat) (cdr lat))
               (makeset (cdr lat)))
     (else (cons (car lat)
                 (makeset (cdr lat)))))))
(define makeset2
  (lambda (lat)
    (cond
     ((null? lat) '())
     (else (cons (car lat)
                 (makeset2
                  (multirember (car lat)
                               (car lat))))))))
