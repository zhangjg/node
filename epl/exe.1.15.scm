(define duple
  (lambda (n sexp)
    (if (zero? n)
        '()
        (cons
         sexp
         (duple
          (- n 1)
          sexp)))))
(duple 2 3)
(duple 4 '(ha ha))
(duple 0 '(blah))