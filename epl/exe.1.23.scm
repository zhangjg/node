(define list-index
  (lambda (pred lst)
    (define t
      (lambda (ret lst)
	(if (eq? lst '())
	    #f
	    (if (pred (car lst))
	    ret
	    (t (+ 1 ret) (cdr lst))))))
    (t 0 lst)))
(list-index number? '(a 2 (1 3) b 7))
(list-index symbol? '(a (b c) 17 foo))
(list-index symbol? '(1 2 (a b) 3))
