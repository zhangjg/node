(define every?
  (lambda (pred lst)
    (define t
      (lambda
	  (ret lst)
	(if (eq? lst '())
	    ret
	    (if (pred (car lst))
		(t #t (cdr lst))
		#f))))
    (t #f lst)))
(every? number? '(a b c 3 e))
(every? number? '(1 2 3 4 5))
      
