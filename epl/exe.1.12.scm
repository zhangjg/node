(define subst
  (lambda (new old slist)
    (if (null? slist)
        slist
        (let ((sexp (car slist)))
          (cons
           (if (symbol? sexp)
               (if (eqv? sexp old) new sexp)
               (subst new old sexp))
           (subst new old (cdr slist)))))))

(subst 'aa 'b '(a b (c b) e f))
