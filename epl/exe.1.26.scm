(define up
  (lambda (lst)
    (define cat
      (lambda (list1 list2)
	(if (eq? list1 '())
	    list2
	    (cons (car list1)
		  (cat (cdr list1) list2)))))
    (if (eq? lst '())
	lst
	(let (
	      (ele (car lst))
	      (rest (cdr lst))
	      )
	  (cond
	   ((and
	     (not (eq? ele '()))
	     (not (list? ele)))
	    (cons
	     ele
	     (up rest)))
	   (else
	    (cat ele (up rest))))))))
(up '((1 2) (3 4)))
(up '((x (y)) z))
	     
