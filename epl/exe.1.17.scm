					; down: Listof(sexp)-> Listof(Listof(sexp))
(define down
  (lambda (lst)
    (if (null? lst)
	lst
	(cons (cons (car lst) '())
	      (down (cdr lst))))))
(down '(1 2 3))
(down '((a) (fine) (idea)))
(down '(a (more (complicated)) object))
