(define tem
  (lambda (ele1 ele2 l1 l2)
    (cond
     ((eq? l1 '())
      (if (< ele1 ele2)
	  (cons ele1 (cons ele2 l2))
	  (if (eq? l2 '())
	      (cons ele1 (cons ele2 l2))
	      (cons
	       ele2
	       (tem ele1 (car l2) l1 (cdr l2))))))
     
     ((eq? l2 '())
      (if (< ele1 ele2)
	  (cons ele1 (tem (car l1) ele2 (cdr l1) l2))
	  (cons ele2 (cons ele1 l1))))
     
     ((< ele1 ele2)
      (cons ele1
	    (tem (car l1) ele2 (cdr l1) l2)))
     
     (else
      (cons ele2
	    (tem ele1 (car l2) l1 (cdr l2)))))))
(define
  merge
  (lambda
      (loi1 loi2)
    (cond
     ((eq? loi1 '())
      loi2)
     ((eq? loi2 '())
      loi1)
     (else
      (tem
       (car loi1)
       (car loi2)
       (cdr loi1)
       (cdr loi2)))
     )))
(define
  sort
  (lambda (loi)
    (cond
     ((eq? loi '()) loi)
     ((eq? (cdr loi) '()) loi)
     (else
      (let (
	    (e1 (car loi))
	    (e2 (cadr loi))
	    (rest (cddr loi)))
	(if (eq? rest '())
	    (if (< e1 e2)
		(cons e1 (cons e2 rest))
		(cons e2 (cons e1 rest)))
	    (let ((e3 (car rest))
		  (rest (cdr rest)))
	      (if (eq? rest '())
		  (if (< e1 e2)
		      (tem e1 e3 (cons e2 '()) rest)
		      (tem e2 e3 (cons e1 '()) rest))
		  (if (< e1 e2)
		      (merge
		       (tem e1 e3 (cons e2 '()) '())
		       (sort rest))
		      (merge
		       (tem e2 e3 (cons e1 '()) '())
		       (sort rest)))))
	 ))
      ))))
(sort '(8 2 5 2 3))
