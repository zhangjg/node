					; swapper : Sym X Sym X Listof(Sym) -> Listof(Sym)
(define swapper
  (lambda (s1 s2 slist)
    (if (null? slist)
	'()
	(let ((sexp (car slist))
              (rest (cdr slist)))
	  (if (symbol? sexp)
              (if (eqv? s1 sexp)
		 (cons s2 (swapper s1 s2 rest))
                 (if (eqv? s2 sexp)
		    (cons s1 (swapper s1 s2 rest))
		    (cons sexp (swapper s1 s2 rest))))
	     (cons (swapper s1 s2 sexp)
                   (swapper s1 s2 rest)))))))

(swapper 'a 'd '(a b c d))
(swapper 'a 'd '(a d () c d))
(swapper 'x 'y '((x) y (z (x))))
