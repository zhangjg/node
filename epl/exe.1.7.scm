(define nth-element 
   (lambda (lst n)
     (if (< (length lst) n)
       (report-list-too-short lst n)
       (if (zero? n)
          (car lst)
          (nth-element (cdr lst) (- n 1))))))
(define report-list-too-short
  (lambda (lst n)
    (error 'nth-element
		"~s does not have ~s elements.~%" lst n)))

(nth-element '(a b c) 8)