(define subst
    (lambda (new old slist)
      (map
       (lambda (sexp)
         (if (symbol? sexp)
             (if (eqv? sexp old)
                 new
                 sexp)
             (subst new old sexp)
             ))
           slist)))

    
(subst 'aa 'b '(a b (a b c) d ))