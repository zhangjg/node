(define invert
   (lambda (lst)
    (if (null? lst)
      '()
      (let ((tow-list (car lst)))
       (cons 
         (cons
          (cadr tow-list)
          (cons (car tow-list) '()))
         (invert (cdr lst)))))))
(invert '((a 1) (a 2) (1 b) (2 b)))
