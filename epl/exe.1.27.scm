(define flatten
  (lambda (slist)
    (define cat
      (lambda (l1 l2)
	(if (eq? l1 '())
	    l2
	    (cons (car l1) l2))))
    (if (eq? slist '())
	slist
	(let (
	      (ele (car slist))
	      (rest (cdr slist))
	      )
	  (if (eq? ele '())
	      (flatten rest)
	      (if (list? ele)
		  (cat
		   (flatten ele)
		   (flatten rest))
		  (cons ele (flatten rest)));ele is atom
		  )
	 )
        )))
(flatten '(a b c))
(flatten '((a) () (b ()) () (c)))
(flatten '((a b) c (((d)) e)))
(flatten '(a b (() (c))))
