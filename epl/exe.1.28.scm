(define tem
  (lambda (ele1 ele2 l1 l2)
    (cond
     ((eq? l1 '())
      (if (< ele1 ele2)
	  (cons ele1 (cons ele2 l2))
	  (if (eq? l2 '())
	      (cons ele1 (cons ele2 l2))
	      (cons
	       ele2
	       (tem ele1 (car l2) l1 (cdr l2))))))
     
     ((eq? l2 '())
      (if (< ele1 ele2)
	  (cons ele1 (tem (car l1) ele2 (cdr l1) l2))
	  (cons ele2 (cons ele1 l1))))
     
     ((< ele1 ele2)
      (cons ele1
	    (tem (car l1) ele2 (cdr l1) l2)))
     
     (else
      (cons ele2
	    (tem ele1 (car l2) l1 (cdr l2)))))))
(define
  merge
  (lambda
      (loi1 loi2)
    (cond
     ((eq? loi1 '())
      loi2)
     ((eq? loi2 '())
      loi1)
     (else
      (tem
       (car loi1)
       (car loi2)
       (cdr loi1)
       (cdr loi2)))
     )))
(merge '(1 4) '(1 2 8))
(merge '(35 62 81 90 91) '(3 83 85 90))
