					; count-occurrences : Sym X ListOf(Sym) -> Int
(define count-occurrences
  (lambda (s slist)
    (if (null? slist)
	0
	(let ((sexp (car slist))
	      (rest (cdr slist)))
	  (cond
	   ((and (symbol? sexp) (eqv? sexp s))
	    (+ 1 (count-occurrences s rest)))
	   ((symbol? sexp)
	    (count-occurrences s rest))
	   (else (+ (count-occurrences s sexp)
		    (count-occurrences s rest))))))))

(count-occurrences 'x '((f x) y (((x z) x))))
(count-occurrences 'x '((f x) y (((x z) () x))))
(count-occurrences 'w '((f x) y (((x z) x))))
	     
