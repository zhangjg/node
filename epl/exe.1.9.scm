;remove : Sym x Listof(Sym) -> Listof(Sym)
;Usage: (remove s los) remove all /s/ in the los.
(define remove
  (lambda (s los)
    (if (null? los)
        los
        (if (eqv? s (car los))
            (remove s (cdr los))
            (cons (car los) (remove s (cdr los)))))))

(remove 'a '(a b a c d a e))
