					;product :  ListOf(Sym) X ListOf(Sym) -> ListOF((Sym Sym))
(define product
  (lambda
      (sos1 sos2)
    (cond
     (
      (eq? sos1 '())
      '())
     (
      (and
       (eq? (cdr sos1) '());sos1 is '(a)
       (not (eq? sos2 '()));sos1 is '(A)
       (eq? (cdr sos2) '())
       ); (a) x (A) 
      (cons
       (cons (car sos1) sos2) ;(a A)
       (product (cdr sos1) sos2)))
     (
      (and (eq? (cdr sos1) '());sos1 is '(a)
	   (not (eq? sos2 '()))
	   (not (eq? (cdr sos2) '()))); (a) x (A B ...) = 
      (cons
       (cons; (a A)
	(car sos1)
	(cons (car sos2) '()))
       (product sos1 (cdr sos2))))
     (
      (and (not (eq? (cdr sos1) '()));sos1 is '(a b ...)
	   (not (eq? sos2 '()))
	   (eq? (cdr sos2) '()));sos2 is '(A); (a b) x (A) 
      (cons
       (cons (car sos1) sos2)
       (product (cdr sos1) sos2)
       ))
     (else
      (cat (product (cons (car sos1) '()) sos2)
           (product (cdr sos1) sos2))
      ))
     ))

(define
  cat
  (lambda
      (t l)
    (if (eq? t '())
        l
        (cons (car t) (cat (cdr t) l)))
    ))

(product '(a b c) '(x y))	  

      
